package com.example.ejemplolistview01;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener {
	
	static final String[] valores = new String[] {"�rbol","casa","televisi�n","sill�n","vidrio","puerta","mesa","foco","silla","l�mpara","sof�","suelo"};
	ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        lv = (ListView) findViewById(R.id.lv1);
        
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,android.R.id.text1,valores);
        
        lv.setAdapter(adaptador);
        lv.setOnItemClickListener(this);
    }
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "hiciste click en: "+arg2, Toast.LENGTH_LONG).show();
	}

    
}
