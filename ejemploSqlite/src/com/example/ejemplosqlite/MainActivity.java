package com.example.ejemplosqlite;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	
	EditText et;
	Button bt,bt2,bt3;
	String usuario;
	TextView tv1,tv2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		et = (EditText) findViewById(R.id.et);
		bt = (Button) findViewById(R.id.bt);
		bt2 = (Button) findViewById(R.id.bt2);
		bt3 = (Button) findViewById(R.id.bt3);
		tv1 = (TextView) findViewById(R.id.tv1);
		tv2 = (TextView) findViewById(R.id.tv2);
		
		
		bt.setOnClickListener(this);
		bt2.setOnClickListener(this);
		bt3.setOnClickListener(this);
		
	}

	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bt:
			DatabaseHelper myDBHelper = new DatabaseHelper(this);
			usuario = et.getText().toString();
			
			try {
				myDBHelper.createDataBase();
			} catch (Exception e) {
				throw new Error("No se cre� la DB");
				// TODO: handle exception
			}
			
			try {
				myDBHelper.openDataBase();
				SQLiteDatabase db = myDBHelper.getWritableDatabase();
				ContentValues valores = new ContentValues();
				valores.put("dni", "Z009");
				valores.put("nombre", usuario);
				valores.put("apellido", "ZURR�N");
				
				
				db.insert("usuarios", null, valores);
				db.close();
				myDBHelper.close();
				
			} catch (SQLException sqle) {
				//throw sqle;
				throw new Error("No se agreg� n� de n�");
				// TODO: handle exception
			}
			
			break;
			
		case R.id.bt2:
			DatabaseHelper myDBHelper2 = new DatabaseHelper(this);
			usuario = et.getText().toString();
			
			try {
				myDBHelper2.createDataBase();
			} catch (Exception ioe) {
				// TODO: handle exception
				throw new Error("No se pudo crear la DB");
			}
			
			try {
				myDBHelper2.openDataBase();
				Cursor cursor = myDBHelper2.buscarUsuario(usuario);
				
				if(cursor !=null) {
					tv1.setText(cursor.getString(0));
					tv2.setText(cursor.getString(1));
					//cursor.close();
					//myDBHelper2.close();
				}
				
				cursor.close();
				myDBHelper2.close();				
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			break;
			
		case R.id.bt3:
			DatabaseHelper myDBHelper3 = new DatabaseHelper(this);
			usuario = et.getText().toString();
			
			try {
				myDBHelper3.createDataBase();
			} catch (Exception ioe) {
				// TODO: handle exception
				throw new Error("No se pudo crear la DB");
			}
			
			try {
				myDBHelper3.openDataBase();
				SQLiteDatabase db = myDBHelper3.getWritableDatabase();
				String where = "nombre = '"+usuario+"'";
				
				db.delete("usuarios", where, null);
				db.close();
				myDBHelper3.close();
				
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			break;

		
		}
	}

	

}
