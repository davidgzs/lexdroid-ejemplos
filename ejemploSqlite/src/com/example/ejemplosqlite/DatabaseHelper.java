package com.example.ejemplosqlite;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static String DB_PATH = "data/data/com.example.ejemplosqlite/databases/";
	static final String DB_NAME = "ejemplo_usuarios.sqlite";
	
	private final Context myContext;
	private SQLiteDatabase myDatabase;

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, 1);
		// TODO Auto-generated constructor stub
		this.myContext = context;
	}

	public void createDataBase() {
		
		boolean dbExist = checkDataBase();
		SQLiteDatabase db_Read = null;
		
		if (dbExist){
			
		}else{
			db_Read = this.getReadableDatabase();
			db_Read.close();
			
			try {
				copyDataBase();
			} catch (Exception e) {
				// TODO: handle exception
				throw new Error("Error copiando Bases de Datos");
			}
		}
	}
	
	
	public void copyDataBase() throws IOException{
		InputStream myInput = myContext.getAssets().open(DB_NAME);
		String outFileName = DB_PATH+DB_NAME;
		
		OutputStream myOutput = new FileOutputStream(outFileName);
		
		byte[] buffer = new byte [1024];
		int length;
		
		while((length = myInput.read(buffer)) != -1){
			if(length > 0){
				myOutput.write(buffer, 0, length);
			}
		}
		
		myInput.close();
		myOutput.flush();
		myOutput.close();
		
	}
	
	
	public boolean checkDataBase(){
		
		SQLiteDatabase checkDB = null;
		//SQLiteDatabase db_Read = null;
		
		try {
			String myPath = DB_PATH+DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
			
		} catch (Exception e) {
			// TODO: handle exception
			File dbFile = new File(DB_PATH+DB_NAME);
			return dbFile.exists();
		}
		
		if(checkDB != null){
			
			checkDB.close();
		}
		
		return checkDB != null ? true : false ;
	}
	
	public void openDataBase(){
		String myPath = DB_PATH+DB_NAME;
		myDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

	public Cursor buscarUsuario(String usuario) {
		
		String[] campos = new String[] {"dni", "apellido"};
		String [] args = new String [] {usuario};
		
		Cursor qCursor = myDatabase.query("usuarios", campos, "nombre=?", args, null, null, null);
		
		if (qCursor != null) {
			qCursor.moveToFirst();
		}
		
		return qCursor;
		
	}
	
}
