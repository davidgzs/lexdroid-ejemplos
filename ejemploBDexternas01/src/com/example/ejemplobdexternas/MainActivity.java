package com.example.ejemplobdexternas;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.R.string;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	EditText us,ps;
	Button bt;
	String usuario,password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		us = (EditText) findViewById(R.id.usuario);
		ps = (EditText) findViewById(R.id.password);
		bt = (Button) findViewById(R.id.bt1);
		
		bt.setOnClickListener(this);
	}// final del OnCreate

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch (v.getId()) {
		case R.id.bt1:
			
			usuario = us.getText().toString();
			password = ps.getText().toString();
			
			ArrayList parametros = new ArrayList();
			parametros.add("usuario");
			parametros.add(usuario);
			parametros.add("password");
			parametros.add(password);
			
			try {
				Post post = new Post();
				JSONArray jsonArray = post.getServerData(parametros, "http://www.imagenatio.com/wp-content/themes/twentytwelve/pruebas/android_login.php");
				//para probar: usuario = antonio o benito / contrase�a = antonio o benito
				if ((jsonArray != null) && (jsonArray.length() > 0)) {
					JSONObject json_data = jsonArray.getJSONObject(0);
					
					int numRegistrados = json_data.getInt("id");
					
					if (numRegistrados > 0) {
						Toast.makeText(getBaseContext(), "Usuario y contrase�a correctos", Toast.LENGTH_SHORT).show();
					}
				}else {
					Toast.makeText(getBaseContext(), "Usuario y/o contrase�a incorrectos", Toast.LENGTH_SHORT).show();
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
				
				Toast.makeText(getBaseContext(), "Error al conectar", Toast.LENGTH_SHORT).show();
				
			}//final del try
			
			break;

		default:
			break;
		}// final del switch
		
	}// final del OnClick

class Post {
	
	private InputStream is = null;
	private String respuesta = "";
	
	private void conectaPost(ArrayList parametros, String URL) {
		ArrayList nameValuesPairs;
		
		try {			
			HttpClient httpClient = new  DefaultHttpClient();
			HttpPost httpPost = new HttpPost(URL);
			nameValuesPairs = new ArrayList();
			
			if(parametros != null) {
				for (int i = 0; i < parametros.size() -1; i+=2) {
					nameValuesPairs.add(new BasicNameValuePair((String)parametros.get(i), (String)parametros.get(i+1)));
				}// fin del for
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuesPairs));
			}// fin de parametros != null
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			
			is = entity.getContent();
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Log Tag", "Error en http conexi�n"+ e.toString());
			
		}//final del try / catch
		
	}// final de conectaPost
	
	private void getRespuestaPost() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line+"\n");
				
			}//final del while
			is.close();
			respuesta  = sb.toString();
			Log.e("Log Tag2", "Cadena Json"+respuesta);
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Log Tag2", "Error tomando el resultado"+e.toString());
			
		}//final del try / catch
		
	}// final del getRespuestaPost
	
	@SuppressWarnings("finally")
	private JSONArray getJsonArray() {
		JSONArray jArray = null;
		
		try {
			jArray = new JSONArray(respuesta);
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Log Tag3", "no se pudo "+e.toString());
			
		}// final del try / catch
		finally {
			return jArray;
			
		}// final del finally
		
	}// final de getJsonArray
	
	public JSONArray getServerData(ArrayList parametros, String URL) {
		conectaPost(parametros, URL);
		if(is != null) {
			getRespuestaPost();
		}
		if((respuesta != null) && (respuesta.trim() != "")) {
			return getJsonArray();
		}else{
			return null;
		}
		
	}// final del getServerData
	
}// final de la clase Post

}// final de la Activity