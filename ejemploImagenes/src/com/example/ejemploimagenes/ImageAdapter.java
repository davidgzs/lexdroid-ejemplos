package com.example.ejemploimagenes;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
	int mGalleryItemBackgrond;
	private Context mContext;
	
	private Integer[] mImageIds = {
			R.drawable.cubo,
			R.drawable.cubo03,
			R.drawable.cubo04,
			R.drawable.cubo02,
			R.drawable.cubo04,
	};
	
	public ImageAdapter(Context c){
		mContext = c;
		TypedArray attr = mContext.obtainStyledAttributes(R.styleable.galeriastilo);
		mGalleryItemBackgrond = attr.getResourceId(R.styleable.galeriastilo_android_galleryItemBackground, 0);
		attr.recycle();
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mImageIds.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView imageView = new ImageView(mContext);
		
		imageView.setImageResource(mImageIds[position]);
		imageView.setLayoutParams(new Gallery.LayoutParams(150,150));
		imageView.setScaleType(ImageView.ScaleType.FIT_XY);
		imageView.setBackgroundResource(mGalleryItemBackgrond);
		
		return imageView;
	}

}	
