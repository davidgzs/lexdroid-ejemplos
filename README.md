LexDroid-Ejemplos
=================

Proyectos Android - Ejemplos de las clases del curso de EscuelaIT "LexDroid" de DesarrolloWeb.com,

Repositorio que almacenará los diversos ejercicios propuestos durante el curso de la iniciativa de EscuelaIT
del portal DesarrolloWeb.com denominada "Laboratorio Avanzado de Desarrollo de Apps en Android" 

Por David Gozalez Saiz (davidgzs@gmail.com)

(Disponible para su acceso y uso "libremente" segun la actual licencia ver "LICENSE.txt")