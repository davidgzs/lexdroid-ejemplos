package com.example.ejemplosonidos02;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	Button bt1,bt2;
	MediaPlayer mp;
	public int flujo=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        bt1 = (Button) findViewById(R.id.bt1);
        bt2 = (Button) findViewById(R.id.bt2);
        
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
    
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bt1:
			play_mp();
			
			break;
		case R.id.bt2:
			stop_mp();
			break;

		default:
			break;
		}
	}
		private void play_mp(){
			mp = MediaPlayer.create(this, R.raw.prueba2);
			mp.start();
		}
		
		private void stop_mp(){
			mp.stop();
		}
  
      
}
