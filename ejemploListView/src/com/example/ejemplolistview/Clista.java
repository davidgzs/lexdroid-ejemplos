package com.example.ejemplolistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Clista extends ArrayAdapter<String> {
	
	private final Context context;
	private final String[] valores;

	public Clista(Context context, String[] valores) {
		super(context, R.layout.lista, valores);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.valores = valores;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.lista,parent,false);
		TextView textview = (TextView) rowView.findViewById(R.id.tx);
		ImageView imageview = (ImageView) rowView.findViewById(R.id.logo);
		
		String s = valores[position];
		System.out.print(s);
		textview.setText(valores[position]);
		
		imageview.setImageResource(R.drawable.ic_launcher);
		return rowView;
		
	}

}
