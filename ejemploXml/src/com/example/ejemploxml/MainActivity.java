package com.example.ejemploxml;

import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.w3c.dom.Element;

import android.net.ParseException;
import android.os.Bundle;
import android.app.Activity;

import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	TextView dni[];
	TextView nombre [];
	
	LinearLayout layout = new LinearLayout(this);
	layout.setOrientation(1);
	
	try {
		URL url = new URL("http://www.ockhamti.com/usuarios.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new InputSource(url.openStream()));
		doc.getDocumentElement().normalize();
		
		NodeList nodeLista = doc.getElementsByTagName("item");
		
		dni = new TextView[nodeLista.getLength()];
		nombre = new TextView[nodeLista.getLength()];
		
		for (int i = 0; i < nodeLista.getLength(); i++) {
			Node node = nodeLista.item(i);
			
			dni[i] = new TextView(this);
			nombre[i] = new TextView(this);
			
			Element fstElemnt = (Element) node;
			
			NodeList dniList = fstElemnt.getElementsByTagName("dni");
			Element dniElement = (Element) dniList.item(0);
			dniList = ((Node) dniElement).getChildNodes();
			dni[i].setText("El DNI es:"+((Node) dniList.item(0)).getNodeValue());
			
			NodeList nombreList = fstElemnt.getElementsByTagName("nombre");
			Element nombreElement = (Element) nombreList.item(0);
			nombreList = ((Node) nombreElement).getChildNodes();
			nombre[i].setText("El Nombre es:"+((Node) nombreList.item(0)).getNodeValue());
			
			layout.addView(dni[i]);
			layout.addView(nombre[i]);
			
		}
		
	} catch (Exception pe) {
		// TODO: handle exception
		System.out.println("XML parse exception"+pe);
	}
	
	setContentView(layout);

	}

}
