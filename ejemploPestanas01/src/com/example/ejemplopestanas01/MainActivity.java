package com.example.ejemplopestanas01;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class MainActivity extends Activity {

	TabHost tabHost;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tabHost = (TabHost) findViewById(R.id.th);
		tabHost.setup();
		
		TabSpec spec = tabHost.newTabSpec("tag 1");
		spec.setIndicator("Num 1", getResources().getDrawable(R.drawable.ic_launcher));
		spec.setContent(R.id.tab1);
		tabHost.addTab(spec);
		
		spec = tabHost.newTabSpec("tag 2");
		spec.setIndicator("Num 2");
		spec.setContent(R.id.tab2);
		tabHost.addTab(spec);
		
		spec = tabHost.newTabSpec("tag 3");
		spec.setIndicator("Num 3", getResources().getDrawable(R.drawable.ic_launcher));
		spec.setContent(R.id.tab3);
		tabHost.addTab(spec);
		
		spec = tabHost.newTabSpec("tag 4");
		spec.setIndicator("Num 4");
		spec.setContent(R.id.tab4);
		tabHost.addTab(spec);
		
		for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
			tabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.BLACK);
		}
		
		//tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.BLACK);
		//tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.BLACK);
		//tabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.RED);
		//tabHost.getTabWidget().getChildAt(3).setBackgroundColor(Color.BLACK);
		
	}

	

}
