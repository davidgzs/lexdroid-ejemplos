package com.example.ejemplotextfield;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends Activity {

	AutoCompleteTextView auto;
	//String[] items = {"Andr�s","Antonio","�lvaro","Antonia","Antonino","Alfredo","Anselmo"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        auto= (AutoCompleteTextView) findViewById(R.id.autotv);
        
        String[] meses = getResources().getStringArray(R.array.meses);
        
        ArrayAdapter<String> adapter = new
        		ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1,meses);
        
        auto.setAdapter(adapter);
    }


    
    
}
